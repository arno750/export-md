package action

import (
	"export-md/model"
	"export-md/util"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// Export from a source folder to a destination folder
// taking into account the target client
func Export(sourcePath string, exportPath string, targetClient string) {
	files, err := Walk(sourcePath)
	if err != nil {
		log.Fatalf("Error while browsing source folder: %v", err)
	}

	err = PrepareExport(files, targetClient)
	if err != nil {
		log.Fatalf("Error while preparing export: %v", err)
	}

	err = ExportFiles(files, exportPath)
	if err != nil {
		log.Fatalf("Error while exporting: %v", err)
	}
}

// Walk source folder and build the structure
func Walk(rootPath string) ([]*model.File, error) {
	rootPath = filepath.Clean(rootPath)
	files := make([]*model.File, 0)
	err := filepath.WalkDir(rootPath, func(path string, d fs.DirEntry, err error) error {
		if !d.IsDir() {
			relativePath := path[len(rootPath)+1:]
			if !strings.HasPrefix(relativePath, ".") && !strings.HasPrefix(d.Name(), ".") {
				files = append(files, &model.File{
					Path:         path,
					RelativePath: relativePath,
					Name:         d.Name(),
					Markdown:     model.IsMarkdownFile(path),
					Exported:     false,
				})
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}

// Assess export status for markdown files
// Determine unnecessary files, clean broken links
func PrepareExport(files []*model.File, targetClient string) error {
	for _, file := range files {
		if file.Markdown {
			err := file.WorkOutExportStatus(targetClient)
			if err != nil {
				return err
			}
			if file.Exported {
				err = file.ReadContent()
				if err != nil {
					return err
				}

				err = file.RemoveMetadata()
				if err != nil {
					return err
				}
			}
		}
	}

	for _, file := range files {
		if file.Markdown && file.Exported {
			WorkOutDataFilesExportedStatus(file, files)
			err := RemoveLinkToUnexportedMarkdownFiles(file, files)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Work out whether data files are needed by the specified markdown file
func WorkOutDataFilesExportedStatus(markdown *model.File, files []*model.File) {
	for _, file := range files {
		if !file.Markdown && !file.Exported {
			file.Exported = markdown.HasLinkTowards(file.Name)
		}
	}
}

// Remove any link to unexported markdown files in the specified markdown file
func RemoveLinkToUnexportedMarkdownFiles(markdown *model.File, files []*model.File) error {
	for _, file := range files {
		if file.Markdown && !file.Exported {
			err := markdown.RemoveLinkTo(file.Name)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Export
func ExportFiles(files []*model.File, exportPath string) error {
	for _, file := range files {
		log.Printf("%v: md(%v) export(%v)", file.Path, file.Markdown, file.Exported)
		if file.Exported {
			destinationPath := filepath.Join(exportPath, file.RelativePath)
			err := os.MkdirAll(filepath.Dir(destinationPath), fs.ModeDir|0755)
			if err != nil {
				return err
			}
			if file.Markdown {
				err = os.WriteFile(destinationPath, []byte(file.Content), 0664)
				if err != nil {
					return err
				}
				log.Printf("  Write markdown file %v", destinationPath)
			} else {
				_, err = util.Copy(file.Path, destinationPath)
				if err != nil {
					return err
				}
				log.Printf("  Write data file %v", destinationPath)
			}
		}
	}

	return nil
}
