package action

import (
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestWalk_givenRelativePath(t *testing.T) {
	testFolder := "../testdata"
	files, err := Walk(testFolder)
	if err != nil {
		t.Errorf("Walk(): unexpected error %v", err)
	}

	for _, file := range files {
		if strings.HasPrefix(file.Name, ".") {
			t.Errorf("Walk(): unexpected hidden file %v/%v", file.Path, file.Name)
		}
		if strings.HasPrefix(file.RelativePath, ".") {
			t.Errorf("Walk(): unexpected hidden folder %v/%v", file.Path, file.Name)
		}
	}

	if len(files) != 9 {
		t.Errorf("Walk(): actual %v expected %v", len(files), 9)
	}
}

func TestWalk_givenAbsolutePath(t *testing.T) {
	currentFolder, err := os.Getwd()
	if err != nil {
		t.Errorf("Walk(): unexpected error %v", err)
	}
	testFolder := filepath.Join(filepath.Dir(currentFolder), "testdata")
	files, err := Walk(testFolder)
	if err != nil {
		t.Errorf("Walk(): unexpected error %v", err)
	}

	for _, file := range files {
		if strings.HasPrefix(file.Name, ".") {
			t.Errorf("Walk(): unexpected hidden file %v/%v", file.Path, file.Name)
		}
		if strings.HasPrefix(file.RelativePath, ".") {
			t.Errorf("Walk(): unexpected hidden folder %v/%v", file.Path, file.Name)
		}
	}

	if len(files) != 9 {
		t.Errorf("Walk(): actual %v expected %v", len(files), 9)
	}
}
