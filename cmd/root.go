package cmd

import (
	"os"

	"export-md/action"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var (
	SourcePath   string
	ExportPath   string
	TargetClient string

	rootCmd = &cobra.Command{
		Short: "Selective markdown documentation exporter",
		Long: `This tool exports a complete markdown documentation
with necessary media files based on the presence and value of
metadata included on top of markdown files.

Metadata allowing 'client1' and 'client2' looks like the following:

[_metadata_:export]: - "client1,client2"

To allow all client:

[_metadata_:export]: - "all"

It is invisible when rendered and removed on exported files.
Depending on the specified client, the selected
markdown files will be exported along with the linked media files.
The broken links toward unexported markdown files will be
replaced by the text part of the link.`,
		Run: func(cmd *cobra.Command, args []string) {
			action.Export(SourcePath, ExportPath, TargetClient)
		},
	}
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVarP(&SourcePath, "source", "s", "", "Path of the source folder (required)")
	// nolint:errcheck
	rootCmd.MarkPersistentFlagRequired("source")
	rootCmd.PersistentFlags().StringVarP(&ExportPath, "export", "e", "", "Path of the export folder (required)")
	// nolint:errcheck
	rootCmd.MarkPersistentFlagRequired("export")
	rootCmd.PersistentFlags().StringVarP(&TargetClient, "client", "c", "", "Target client (required)")
	// nolint:errcheck
	rootCmd.MarkPersistentFlagRequired("client")
}
