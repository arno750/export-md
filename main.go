/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "export-md/cmd"

func main() {
	cmd.Execute()
}
