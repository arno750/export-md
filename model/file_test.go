package model

import (
	"testing"
)

func TestIsMarkdownFile(t *testing.T) {
	if IsMarkdownFile("file.png") {
		t.Errorf("IsMarkdownFile(): markdown not expected!")
	}
	if !IsMarkdownFile("file.md") {
		t.Errorf("IsMarkdownFile(): markdown expected!")
	}
}

func TestWorkOutExportStatus_givenExportable(t *testing.T) {
	file := File{
		Path: "../testdata/child1.md",
	}
	err := file.WorkOutExportStatus("any")
	if err != nil {
		t.Errorf("WorkOutExportStatus(): unexpected error %v", err)
	}
	if !file.Exported {
		t.Errorf("WorkOutExportStatus(): exported expected!")
	}
}

func TestWorkOutExportStatus_givenNotExportable(t *testing.T) {
	file := File{
		Path: "../testdata/child3.md",
	}
	err := file.WorkOutExportStatus("any")
	if err != nil {
		t.Errorf("WorkOutExportStatus(): unexpected error %v", err)
	}
	if file.Exported {
		t.Errorf("WorkOutExportStatus(): not exported expected!")
	}
}

func TestWorkOutExportStatus_givenSpecialExport(t *testing.T) {
	file := File{
		Path: "../testdata/sub/child4.md",
	}
	err := file.WorkOutExportStatus("any")
	if err != nil {
		t.Errorf("WorkOutExportStatus(): unexpected error %v", err)
	}
	if file.Exported {
		t.Errorf("WorkOutExportStatus(): not exported expected!")
	}

	err = file.WorkOutExportStatus("special")
	if err != nil {
		t.Errorf("WorkOutExportStatus(): unexpected error %v", err)
	}
	if !file.Exported {
		t.Errorf("WorkOutExportStatus(): exported expected!")
	}
}

func TestWorkOutExportStatus_givenUnexistingFile(t *testing.T) {
	file := File{
		Path: "../testdata/child5.md",
	}
	err := file.WorkOutExportStatus("any")
	if err == nil {
		t.Errorf("WorkOutExportStatus(): error expected!")
	}
}

func TestReadContent(t *testing.T) {
	file := File{
		Path: "../testdata/child1.md",
	}
	err := file.ReadContent()
	if err != nil {
		t.Errorf("ReadContent(): unexpected error %v", err)
	}

	expected := `[metadata.export]: #"all"

# child1

[Back to parent](parent1.md)

![Close](close.png)
`
	actual := file.Content
	if actual != expected {
		t.Errorf("ReadContent(): actual\n%v\n/ expected\n%v", actual, expected)
	}
}

func TestReadContent_givenUnexistingFile(t *testing.T) {
	file := File{
		Path: "../testdata/child5.md",
	}
	err := file.ReadContent()
	if err == nil {
		t.Errorf("ReadContent(): error expected!")
	}
}

func TestRemoveMetadata(t *testing.T) {
	file := File{
		Content: `[metadata.export]: #"all"

## child1
[Back to parent](parent1.md)
![Close](close.png)
`,
	}

	err := file.RemoveMetadata()
	if err != nil {
		t.Errorf("RemoveMetadata(): unexpected error %v", err)
	}

	expected := `
## child1
[Back to parent](parent1.md)
![Close](close.png)
`
	actual := file.Content
	if actual != expected {
		t.Errorf("RemoveMetadata(): actual\n%v\n/ expected\n%v", actual, expected)
	}
}

func TestRemoveMetadata_givenOneLine(t *testing.T) {
	file := File{
		Content: "dummy",
	}
	err := file.RemoveMetadata()
	if err == nil {
		t.Errorf("ReadContent(): error expected")
	}
}

func TestRemoveMetadata_givenNoMetadata(t *testing.T) {
	file := File{
		Content: `## dummy
content`,
	}
	err := file.RemoveMetadata()
	if err == nil {
		t.Errorf("ReadContent(): error expected")
	}
}

func TestHasLinkTowards(t *testing.T) {
	file := File{
		Content: `## example
(opening
[Back to parent](../parent1.md)
![Down](down.png)
![Close](../close.png "Close")
`,
	}

	if file.HasLinkTowards("up.png") {
		t.Errorf("HasLinkTowards(): false expected")
	}
	if file.HasLinkTowards("example") {
		t.Errorf("HasLinkTowards(): false expected")
	}
	if file.HasLinkTowards("opening") {
		t.Errorf("HasLinkTowards(): false expected")
	}
	if !file.HasLinkTowards("down.png") {
		t.Errorf("HasLinkTowards(): true expected")
	}
	if !file.HasLinkTowards("close.png") {
		t.Errorf("HasLinkTowards(): true expected")
	}
}

func TestRemoveLinkTo(t *testing.T) {
	file := File{
		Content: "unexported [link 3](child3.md) unexported",
	}

	err := file.RemoveLinkTo("child3.md")
	if err != nil {
		t.Errorf("RemoveMetadata(): unexpected error %v", err)
	}

	expected := "unexported link 3 unexported"
	actual := file.Content
	if actual != expected {
		t.Errorf("RemoveLinkTo(): actual\n%v\n/ expected\n%v", actual, expected)
	}
}

func TestRemoveLinkTo_givenAnchor(t *testing.T) {
	file := File{
		Content: "unexported/section [link 3](child3.md#section-add) unexported/section",
	}

	err := file.RemoveLinkTo("child3.md")
	if err != nil {
		t.Errorf("RemoveMetadata(): unexpected error %v", err)
	}

	expected := "unexported/section link 3 unexported/section"
	actual := file.Content
	if actual != expected {
		t.Errorf("RemoveLinkTo(): actual\n%v\n/ expected\n%v", actual, expected)
	}
}

func TestRemoveLinkTo_givenPath(t *testing.T) {
	file := File{
		Content: "unexported/sub [link 3](sub/child3.md) unexported/sub",
	}

	err := file.RemoveLinkTo("child3.md")
	if err != nil {
		t.Errorf("RemoveMetadata(): unexpected error %v", err)
	}

	expected := "unexported/sub link 3 unexported/sub"
	actual := file.Content
	if actual != expected {
		t.Errorf("RemoveLinkTo(): actual\n%v\n/ expected\n%v", actual, expected)
	}
}

func TestRemoveLinkTo_givenNoClosingParenthesisOnAnchorLink(t *testing.T) {
	file := File{
		Content: "unexported/section [link 3](child3.md#section-add unexported/section",
	}

	err := file.RemoveLinkTo("child3.md")
	if err == nil {
		t.Errorf("RemoveMetadata():  error expected")
	}
}

func TestRemoveLinkTo_givenUnexpectedCharacterAfterLink(t *testing.T) {
	file := File{
		Content: "unexported [link 3](child3.md! unexported",
	}

	err := file.RemoveLinkTo("child3.md")
	if err == nil {
		t.Errorf("RemoveMetadata():  error expected")
	}
}

func TestRemoveLinkTo_givenNoTextLinkSeparators(t *testing.T) {
	file := File{
		Content: "unexported [link 3]child3.md) unexported",
	}

	err := file.RemoveLinkTo("child3.md")
	if err == nil {
		t.Errorf("RemoveMetadata():  error expected")
	}
}

func TestRemoveLinkTo_givenNoOpeningBracket(t *testing.T) {
	file := File{
		Content: "unexported link 3](child3.md) unexported",
	}

	err := file.RemoveLinkTo("child3.md")
	if err == nil {
		t.Errorf("RemoveMetadata():  error expected")
	}
}
