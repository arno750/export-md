package model

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	EXPORT_METADATA    = "[metadata.export]"
	METADATA_SEPARATOR = "#"
	ALL                = "all"
)

type File struct {
	Path         string
	RelativePath string
	Name         string
	Markdown     bool
	Exported     bool
	Content      string
}

func IsMarkdownFile(path string) bool {
	return strings.HasSuffix(path, ".md")
}

func (f *File) WorkOutExportStatus(targetClient string) error {
	f.Exported = false
	file, err := os.Open(f.Path)
	if err != nil {
		return err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	line := scanner.Text()
	if strings.Contains(line, EXPORT_METADATA) {
		_, line, found := strings.Cut(line, METADATA_SEPARATOR)
		if !found {
			return nil
		}
		clients := strings.Split(strings.Trim(line, "\" "), ",")
		for _, client := range clients {
			client = strings.TrimSpace(client)
			if (client == ALL) || (client == targetClient) {
				f.Exported = true
				return nil
			}
		}
	}

	return nil
}

func (f *File) ReadContent() error {
	data, err := os.ReadFile(f.Path)
	if err != nil {
		return err
	}
	f.Content = string(data)
	return nil
}

func (f *File) RemoveMetadata() error {
	index := strings.Index(f.Content, "\n")
	if index == -1 {
		return fmt.Errorf("Unexpected content with no LF character!")
	}
	if !strings.Contains(f.Content[:index], EXPORT_METADATA) {
		return fmt.Errorf("Metadata expected on first line!")
	}
	f.Content = f.Content[index+1:]
	return nil
}

func (f *File) HasLinkTowards(name string) bool {
	index := strings.Index(f.Content, name)
	if index > 1 {
		previousCharacter := f.Content[index-1]
		if (previousCharacter != '(') && (previousCharacter != '/') {
			return false
		}
		if index+len(name) < len(f.Content) {
			nextCharacter := f.Content[index+len(name)]
			if nextCharacter == ')' {
				return true
			}
			if strings.HasPrefix(f.Content[index+len(name):], " \"") {
				return true
			}
		}
	}
	return false
}

func (f *File) RemoveLinkTo(name string) error {
	for {
		index := strings.Index(f.Content, name)
		if index == -1 {
			break
		}
		linkEnd := index + len(name)
		switch f.Content[linkEnd] {
		case ')':
			linkEnd++
		case '#':
			closingParenthesis := strings.Index(f.Content[linkEnd:], ")")
			if closingParenthesis == -1 {
				return fmt.Errorf("No closing parenthesis on anchor link %v", name)
			}
			linkEnd = linkEnd + closingParenthesis + 1
		default:
			return fmt.Errorf("Unexpected character after link %v", name)
		}

		beginning := f.Content[0:index]
		linkStart := strings.LastIndex(beginning, "](")
		if linkStart == -1 {
			return fmt.Errorf("No text/link separators ]( for link %v", name)
		}
		titleStart := strings.LastIndex(beginning, "[")
		if titleStart == -1 {
			return fmt.Errorf("No opening bracket for link %v", name)
		}
		log.Printf("  Removing link %v", f.Content[titleStart:linkEnd])
		f.Content = f.Content[:titleStart] + f.Content[titleStart+1:linkStart] + f.Content[linkEnd:]
	}
	return nil
}
