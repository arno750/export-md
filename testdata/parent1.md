[metadata.export]: #"all"

# Parent

exported [link 1](child1.md) exported

unexported [link 3](child3.md) unexported

unexported/section [link 3](child3.md#section-add) unexported/section

exported/sub [link 2](sub/child2.md) exported/sub

unexported/sub [link 4](sub/child4.md) unexported/sub
