# export-md

This tool is a selective markdown documentation exporter.

It exports a complete markdown documentation with necessary media files based on the presence and value of metadata included on top of markdown files.

Metadata allowing `client1` and `client2` looks like the following:

```
[_metadata_:export]: - "client1,client2"
```

To allow all client:

```
[_metadata_:export]: - "all"
```

It is invisible when rendered and removed on exported files.

Depending on the specified client, the selected
markdown files will be exported along with the linked media files.
The broken links toward unexported markdown files will be
replaced by the text part of the link.

## Support

[open an issue here](https://gitlab.com/arno750/export-md/-/issues).

## Authors & contributors

Original setup of this repository by [Arnaud WIELAND](https://gitlab.com/arno750).


## License

Every details about licence are in a [separate document](LICENSE).

## About the project

### Build

```bash
make
```

### Example

```bash
./export-md -s ./testdata -e ./output -c any
```
